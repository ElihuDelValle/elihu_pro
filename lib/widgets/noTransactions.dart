import 'package:flutter/material.dart';

class NoTransactions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Column(
        children: [
          Container(
            child: Text("You haven't spent any money",
                style: Theme.of(context).textTheme.headline6),
            margin: EdgeInsets.all(20),
          ),
          Container(
            child: Image.asset(
              'assets/images/noMoney.png',
              fit: BoxFit.cover,
            ),
            height: constraints.maxHeight * 0.7,
            alignment: Alignment.topCenter,
          )
        ],
      );
    });
  }
}
