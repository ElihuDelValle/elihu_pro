import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  final Function addTransaction;

  NewTransaction(this.addTransaction);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime _datePicked;

  void _submitData() {
    if (_titleController.text.isEmpty ||
        double.parse(_amountController.text) <= 0 ||
        _datePicked == null) {
      return;
    }
    widget.addTransaction(_titleController.text,
        double.parse(_amountController.text), _datePicked);
    Navigator.of(context).pop();
  }

  void _showDatePicker(context) {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
    ).then((value) {
      if (value != null) {
        setState(() {
          _datePicked = value;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return SingleChildScrollView(
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        elevation: 10,
        child: Container(
          padding: EdgeInsets.only(
            bottom: mediaQuery.viewInsets.bottom + 10,
            left: 10,
            right: 10,
            top: 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(labelText: "Title"),
                controller: _titleController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Amount"),
                controller: _amountController,
                keyboardType: TextInputType.number,
                onSubmitted: (_) => _submitData(),
              ),
              Container(
                height: 90,
                child: Row(children: <Widget>[
                  Expanded(
                    child: Text(
                      _datePicked != null
                          ? DateFormat('dd MMMM, yyyy').format(_datePicked)
                          : 'No Date Chosen',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  FlatButton(
                    child: Text(
                      'Choose Date',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    textColor: Theme.of(context).primaryColor,
                    onPressed: () => {
                      _showDatePicker(context),
                    },
                  ),
                ]),
              ),
              RaisedButton(
                color: Theme.of(context).accentColor,
                textColor: Theme.of(context).textTheme.button.color,
                onPressed: () => _submitData(),
                child: Text("Add"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
