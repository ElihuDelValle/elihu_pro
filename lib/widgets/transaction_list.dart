import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/transaction.dart';
import './noTransactions.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> _transactions;
  final Function removeTransaction;

  TransactionList(this._transactions, this.removeTransaction);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return _transactions.isEmpty
        ? NoTransactions()
        : ListView.builder(
            itemCount: _transactions.length,
            itemBuilder: (ctx, index) {
              return Card(
                elevation: 3,
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 30,
                    child: Padding(
                      padding: EdgeInsets.all(7),
                      child: FittedBox(
                          child: Text('\$${_transactions[index].amount}')),
                    ),
                  ),
                  title: Text('${_transactions[index].title}'),
                  subtitle: Text(
                    DateFormat('dd MMMM, yyyy')
                        .format(_transactions[index].date),
                    style: TextStyle(color: Colors.grey),
                  ),
                  trailing: mediaQuery.size.width > 500
                      ? FlatButton.icon(
                          onPressed: () =>
                              removeTransaction(_transactions[index].id),
                          icon: Icon(Icons.delete),
                          textColor: Theme.of(context).errorColor,
                          label: Text('Delete'),
                        )
                      : IconButton(
                          icon: Icon(Icons.delete),
                          color: Theme.of(context).errorColor,
                          onPressed: () =>
                              removeTransaction(_transactions[index].id),
                        ),
                ),
              );
            },
          );
  }
}
