import 'package:elihu_pro/widgets/chartBar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/transaction.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransactions;

  Chart(this.recentTransactions);

  List<Map<String, Object>> get recentTransactionsMap {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(Duration(days: index));
      var totalDaySpent = 0.0;

      for (var transaction in recentTransactions) {
        if (transaction.date.day == weekDay.day &&
            transaction.date.month == weekDay.month &&
            transaction.date.year == weekDay.year) {
          totalDaySpent += transaction.amount;
        }
      }

      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalDaySpent
      };
    }).reversed.toList();
  }

  double get totalWeekSpent {
    return recentTransactionsMap.fold(0.0, (previousValue, transactionMap) {
      return previousValue + transactionMap['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 6,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.end,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: recentTransactionsMap.map((dayTransactions) {
            return Expanded(
              child: ChartBar(
                dayTransactions['day'],
                dayTransactions['amount'],
                totalWeekSpent == 0.0
                    ? 0.0
                    : (dayTransactions['amount'] as double) / totalWeekSpent,
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
