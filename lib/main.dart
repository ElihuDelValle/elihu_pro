import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import './models/transaction.dart';
import './widgets/transaction_list.dart';
import './widgets/chart.dart';
import './widgets/new_transaction.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ElihuPro',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        accentColor: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: 'QuickSand',
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                fontFamily: 'QuickSand',
                fontSize: 15,
                color: ThemeData.light().primaryColor,
                fontWeight: FontWeight.bold,
              ),
              bodyText2: TextStyle(
                fontFamily: 'QuickSand',
              ),
              button: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                headline6: TextStyle(
                  fontFamily: 'OpenSans',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
        ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _showChart = false;

  void _openTransactionModal(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (_) {
        return NewTransaction(_addTransaction);
      },
    );
  }

  final List<Transaction> _transactions = [
    Transaction(
      id: "1",
      title: "Yesterday",
      amount: 57.3,
      date: DateTime.now().subtract(Duration(days: 1)),
    ),
    Transaction(
      id: "2",
      title: "2 Days Ago",
      amount: 157.3,
      date: DateTime.now().subtract(Duration(days: 2)),
    ),
  ];

  List<Transaction> get _recentTransactions {
    return _transactions
        .where(
          (transaction) => transaction.date.isAfter(
            DateTime.now().subtract(
              Duration(days: 7),
            ),
          ),
        )
        .toList();
  }

  void _addTransaction(String title, double amount, DateTime date) {
    final newTransactionObject = Transaction(
        title: title,
        amount: amount,
        date: date,
        id: DateTime.now().toString());

    setState(() {
      _transactions.add(newTransactionObject);
    });
  }

  void _removeTransaction(String id) {
    setState(() {
      _transactions.removeWhere((element) => element.id == id);
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    final homeAppBar = AppBar(
      title: Text(
        "ElihuPro",
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () => _openTransactionModal(context),
        ),
      ],
    );

    final landscapeView = mediaQuery.orientation == Orientation.landscape;

    final topBarSize = homeAppBar.preferredSize.height + mediaQuery.padding.top;

    final txListWidget = Container(
      height: (mediaQuery.size.height - topBarSize) * 0.75,
      child: TransactionList(_transactions, _removeTransaction),
    );

    final body = SafeArea(
      child: ListView(
        children: <Widget>[
          if (landscapeView)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Show Chart',
                  style: Theme.of(context).textTheme.headline6,
                ),
                Switch.adaptive(
                  value: _showChart,
                  onChanged: (value) => {
                    setState(() => _showChart = value),
                  },
                )
              ],
            ),
          if (!landscapeView)
            Container(
              height: (mediaQuery.size.height - topBarSize) * 0.25,
              child: Chart(_recentTransactions),
            ),
          if (!landscapeView) txListWidget,
          if (landscapeView)
            _showChart
                ? Container(
                    height: (mediaQuery.size.height - topBarSize) * 0.70,
                    child: Chart(_recentTransactions),
                  )
                : txListWidget
        ],
      ),
    );

    // return !Platform.isIOS
    //     ? CupertinoPageScaffold(
    //         child: body,
    //         navigationBar: homeAppBar,
    //       )
    //     : Scaffold(
    return Scaffold(
      appBar: homeAppBar,
      body: body,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: !Platform.isIOS
          ? FloatingActionButton(
              onPressed: () => _openTransactionModal(context),
              child: Icon(Icons.add),
            )
          : null,
    );
  }
}
